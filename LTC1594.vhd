----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:05:53 04/03/2014 
-- Design Name: sudarshan
-- Module Name:    LTC1594 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity LTC1594 is
    Port ( 
	          sclk : in  STD_LOGIC;
	          clk : in  STD_LOGIC;
			  rst : in  STD_LOGIC;
			  SCLK_LTC1594 : out  STD_LOGIC;
			  MOSI_LTC1594 : out  STD_LOGIC;
			  MISO_LTC1594 : in  STD_LOGIC;
              SS_LTC1594 : out  STD_LOGIC;
              TEMP_DATA_1594_ch1 : out  STD_LOGIC_VECTOR (15 downto 0);
              TEMP_DATA_1594_ch2 : out  STD_LOGIC_VECTOR (15 downto 0);
              TEMP_DATA_1594_ch3 : out  STD_LOGIC_VECTOR (15 downto 0) 
 	       );
         --    
end LTC1594;



	
	
architecture Behavioral of LTC1594 is

	---------------------------------------------------------------------------------

	signal address					: std_logic_vector(9 downto 0);
	signal instruction			: std_logic_vector(17 downto 0);
	signal port_id					: std_logic_vector(7 downto 0);
	signal write_strobe			: std_logic;
	signal out_port				: std_logic_vector(7 downto 0);
	signal read_strobe			: std_logic;
	signal in_port					: std_logic_vector(7 downto 0);
	signal interrupt				: std_logic;
	signal interrupt_ack			: std_logic;
	signal sclk_ltc15941			: std_logic;
	signal cs						: std_logic;
	signal S_DATA_IN				: std_logic_vector(7 downto 0);
--	signal sclk_count				: std_logic_vector(7 downto 0);
	--signal LTC_DATA_LATCH		: std_logic;
	signal INTR_STATUS			: std_logic_vector(7 downto 0);
	signal LTC1594_DATA_LSB		: std_logic_vector(7 downto 0);
	signal LTC1594_DATA_MSB		: std_logic_vector(7 downto 0);
	signal LTC1594_DATA_CTRL	: std_logic_vector(7 downto 0);
	signal channel_address		: std_logic_vector(3 downto 0);
	signal cs_count		   	: std_logic_vector(4 downto 0);
	signal sh_in		      	: std_logic_vector(15 downto 0);
	signal TEMP_DATA_1594_ext	: std_logic_vector(11 downto 0);
	signal initial_channel	   : std_logic_vector(3 downto 0);
	signal count_channel	      : std_logic_vector(3 downto 0);
	signal MOSI_LTC1594_ext    : std_logic_vector(3 downto 0);
	signal LTC_DATA_LATCH_initial    : std_logic;
--	signal sclk_reg1    : std_logic;
	signal sclk_reg2    : std_logic;
	signal sclk_reg3    : std_logic;
	signal sclk_reg4    : std_logic;
	signal sclk_reg5    : std_logic;
	signal sclk_reg6    : std_logic;
	signal sclk_reg7    : std_logic;
	signal sclk_reg8    : std_logic;
	signal sclk_reg9    : std_logic;
	signal sclk_delay    : std_logic;
	signal ssclk    : std_logic;
	signal sck_posedge    : std_logic;
	signal sck_negedge    : std_logic;
	signal en    : std_logic;
	signal en1    : std_logic;
	signal en2    : std_logic;
	signal sclk_ltc1594_int    : std_logic;
	signal sig_cs_mux1    : std_logic;
	signal sclk_by_16_reg1    : std_logic;
	signal sclk_by_16_reg2    : std_logic;
	signal sclk_by_16_reg3    : std_logic;
	signal sclk_by_16_reg4    : std_logic;
	signal SCLK_LTC_cs_reg1    : std_logic;
	signal SCLK_LTC_cs_reg2    : std_logic;
	signal sclk_by_16    : std_logic;
	signal cs_mux1    : std_logic;
	signal cs_mux2    : std_logic;
	signal sclk_reg1    : std_logic;
	signal count_sck				: std_logic_vector(3 downto 0);
	
begin



					
  CLK_BUFG_INST : BUFG
      port map (I=>sclk,
                O=>SCLK_LTC1594);
					 
					 


		process(clk,rst)
		begin
			if(rst='1') then
				sclk_reg1 <='0';
			
			elsif rising_edge(clk) then
				sclk_reg1  <= sclk;
	
			end if;
		end process;
		
			sck_posedge <= not sclk_reg1 and  sclk;
			sck_negedge <=  sclk_reg1 and  not sclk;
		
--   signalout <= sck_posedge;
			
			
	----------------------------------------------------------------------------------------------------------------
	-- sclk_by_16 clock generation
	----------------------------------------------------------------------------------------------------------------
		
		process (sck_posedge,rst)
		begin
			if rst ='1' then
				count_sck   <= (others=>'0');
				sclk_by_16  <= '1';
			elsif	rising_edge(sck_posedge) then
			--	if sck_posedge ='1' then
		    if count_sck = 9 then
			     count_sck   <= (others=>'0');
				 sclk_by_16 <= not sclk_by_16;  --20KHz signal
			 else
				 count_sck <= count_sck + '1';
			 end if;
			--	end if;
			end if;
		end process;




	-----------------------------------------------------------------------------------------------------------------

	-----------------------------------------------------------------------------------------------------------------
	--cs_adc and cs_mux generation.
	-----------------------------------------------------------------------------------------------------------------
		process(sck_posedge,rst)
		begin
			if rst ='1' then
				sclk_by_16_reg1	<='0';
				sclk_by_16_reg2	<='0';
				sclk_by_16_reg3	<='0';
				sclk_by_16_reg4	<='0';

			elsif rising_edge(sck_posedge) then
			--	if sck_posedge ='1' then
				 sclk_by_16_reg1 <=sclk_by_16;
				 sclk_by_16_reg2 <=sclk_by_16_reg1;
				 sclk_by_16_reg3 <=sclk_by_16_reg2;
				 sclk_by_16_reg4 <=sclk_by_16_reg3; --delay of 20us, (4 sclk_LTC1594)
	
			 end if;
			--end if;
		 end process;
 
	 sig_cs_mux1	<=	(not sclk_by_16_reg4 and  sclk_by_16); -- high for 4 sclk_LTC1594 and low for rest

 


 


 SRL16_inst_1 : SRL16
   generic map (
      INIT => X"0000")
   port map (
      Q => cs_mux1,       -- SRL data output -- adds a delay of 16*20ns 
      A0 => '1',     -- Select[0] input
      A1 => '1',     -- Select[1] input
      A2 => '1',     -- Select[2] input
      A3 => '1',     -- Select[3] input
      CLK => CLK,   -- Clock input
      D => sig_cs_mux1        -- SRL data input
   );
	
	
 SRL16_inst_2 : SRL16
   generic map (
      INIT => X"0000")
   port map (
      Q => cs_mux2,       -- SRL data output -- adds a delay of 16*20ns 
      A0 => '1',     -- Select[0] input
      A1 => '1',     -- Select[1] input
      A2 => '1',     -- Select[2] input
      A3 => '1',     -- Select[3] input
      CLK => CLK,   -- Clock input
      D => cs_mux1        -- SRL data input
   );
	
	
 	 
 cs <= cs_mux2;	
 

SS_LTC1594		<= cs;

 en <= '1' when cs = '0' else '0';
--process (sck_posedge,rst)
process (sck_posedge,rst)

	begin
		 
		 if(rst = '1') then
				initial_channel <= "1000";
		--		MOSI_LTC1594_ext <= "1000";
		 elsif (rising_edge(sck_posedge)) then

			 if (count_channel = 15) then -- 
				   
						
				 if (initial_channel = 10) then
					 initial_channel <= "1000";
 
			     else 
				     initial_channel <= initial_channel + '1';

				 end if;	
			 end if;
             
		    					 
		 end if;				
		
   end process;
	
	

	
	
--process (rst,sck_negedge)
process (rst,sck_negedge)
 begin
		 if (rst = '1') then
		     
		     MOSI_LTC1594_ext <= "1000" ;
		     LTC_DATA_LATCH <=  "1000" ;
		 elsif (rising_edge(sck_negedge)) then
		  
		    if (cs = '1') then
				MOSI_LTC1594 <= MOSI_LTC1594_ext(3);
			  --  LTC_DATA_LATCH <= "0011";
		   	    MOSI_LTC1594_ext <= MOSI_LTC1594_ext(2 downto 0) & '0';
		    end if;
				
		    if (count_channel = 15) then
				 MOSI_LTC1594_ext <= initial_channel;
			     LTC_DATA_LATCH <= initial_channel;
		    end if;				
		--	end if;	
		 end if; 		 
 end process; 
--	  
	  

	  
	 
	
--process (rst,sck_posedge)
process (rst,sck_posedge)
 begin
   if (rst = '1') then

		count_channel <= (others => '0'); 
	   
    elsif (rising_edge(sck_posedge)) then
	    if(cs = '0') then
					 			
            if (count_channel = 15) then
		  
		      	count_channel <= "0000";
				 
			else
                count_channel <= count_channel + '1';			
		    end if;		
	    end if;
	

    end if;		
end process;


 


--	process (rst,sck_posedge)
	process (rst,sck_posedge)
 begin
    if (rst = '1') then
		sh_in <= (others => '0');
	   
	elsif (rising_edge(sck_posedge)) then

	    if(cs = '0' ) then
		     sh_in(0) <=  MISO_LTC1594;
		    -- sh_in(0) <=  '1';
            sh_in( 15 downto 1) <= sh_in(14 downto 0);
		
--          sh_in <= x"1010";
		
		 end if;

	end if;		
end process;
 

 process (sck_posedge)
 begin
 
	if (rising_edge(sck_posedge)) then

	   if (count_channel = 15) then
  	        TEMP_DATA_1594_ext <=  sh_in(11 downto 0);
 
	   end if;
		
    end if;
 
end process;

 	
 process (initial_channel,sck_posedge)
begin
  if rising_edge(sck_posedge) then
   case initial_channel is
      when "1000" => TEMP_DATA_1594_ch1 <= TEMP_DATA_1594_ext(11) & TEMP_DATA_1594_ext(11) & TEMP_DATA_1594_ext(11) & TEMP_DATA_1594_ext(11) &  TEMP_DATA_1594_ext;
      when "1001" => TEMP_DATA_1594_ch2 <= TEMP_DATA_1594_ext(11) & TEMP_DATA_1594_ext(11) & TEMP_DATA_1594_ext(11) & TEMP_DATA_1594_ext(11) &  TEMP_DATA_1594_ext;
      when "1010" => TEMP_DATA_1594_ch3 <= TEMP_DATA_1594_ext(11) & TEMP_DATA_1594_ext(11) & TEMP_DATA_1594_ext(11) & TEMP_DATA_1594_ext(11) &  TEMP_DATA_1594_ext;
   --   when "1011" => adc_1594_ch4 <= "0000" & data_out_12;
      when others => TEMP_DATA_1594_ch1 <= (others=>'0');
	    TEMP_DATA_1594_ch2 <= (others=>'0');
	    TEMP_DATA_1594_ch3 <= (others=>'0');
	 
    end case;
  end if;
end process;

  
  
end Behavioral;

